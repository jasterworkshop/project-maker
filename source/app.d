import std.stdio, std.getopt, std.path, std.file;

import maker.data;

import sdlang;

void main(string[] args)
{
    // Get the input
    auto input = getTerminalInput(args);
    if(input.name is null && input.generator is null) return;

    // Then create and populate everything in GeneratorData
    GeneratorData data;
    data.input              = input;
    data.generators         = readGeneratorList();
    data.projectDirectory   = buildNormalizedPath(getcwd(), data.input.name);

    // Read in the libraries we're using
    foreach(path; data.input.libraries)
    {
        data.libraries ~= Library(path);
    }

    // Temporarily placed here.
    // Create a generator
    try
    {
        auto generator      = Generator(data);
        auto constructor    = Constructor(generator);

        constructor.construct();
    }
    catch(Exception ex)
    {
        writeln(ex.msg);
        return;
    }

    /// Write out some debug info
    debug
    {
//        writefln("GeneratorData = %s\n", data);
//        writefln("Generator = %s", generator);
    }
}

TerminalInput getTerminalInput(string[] args)
{
    // Get things
    TerminalInput input = TerminalInput(null, null);
    try
    {
        auto result = args.getopt(
            "name", "Required: The name of the project.", &input.name,

            "generator", "Required: The name of the generator to use.", &input.generator,

            "library", "The name of a library to include. (multiple use allowed)", &input.libraries
            );

        if(result.helpWanted || input.name is null || input.generator is null)
        {
            defaultGetoptPrinter("This program is used to create projects.", result.options);
            return TerminalInput(null, null);
        }
    }
    catch
    {
        writeln("Must supply the --name and --generator options.");
        return TerminalInput(null, null);
    }

    return input;
}