﻿/// Contains code to convert SDL data into certain data structures needed to setup things.
module maker.data;

private
{
    import std.uuid, std.path, std.string, std.range, std.algorithm, std.array, std.file, std.exception;
    import std.stdio : writefln;

    import sdlang;
}

/// Struct containing all of the possible data entered from the terminal.
struct TerminalInput
{
    /// The name of the project.
    string name;

    /// The generator to use.
    string generator;

    /// Library names to use
    string[] libraries;
}

/// Data the a generator may need.
struct GeneratorData
{
    /// Command line options.
    TerminalInput           input;

    /// The generators that the program knows of, keyed by name.
    GeneratorFile[string]   generators;

    /// The directory of the project.
    string                  projectDirectory;

    /// The libraries to include in the project.
    Library[]               libraries;

    /// Convinience method.
    GeneratorFile getGenerator(string name)
    {
        auto ptr = (name in this.generators);
        if(ptr is null)
        {
            throw new Exception("No generator by the name of: " ~ name);
        }
        else
        {
            return *ptr;
        }
    }
}

/// Contains data about a generator file. Found in Generators/List.sdl
struct GeneratorFile
{
    /// The name the user has to use to access this generator.
    string name;
    
    /// The generator's file.
    string file;
}

/// Contains data about a folder that a generator specifies.
struct Folder
{
    /// Path/Name of the folder. May use variables.
    string name;

    /// What type of folder it is.
    /// 
    /// Normal  = No special purpose(Default).
    /// Libs    = This folder is where the library files should be stored.
    /// LibCode = This folder is where the code for the libraries is stored.
    /// Bin     = This folder is where an exe will be made, all shared objects are copied here.
    string type = "Normal";
}

/// Contains data about a file that the generator wants to create.
struct File
{
    /// Path/Name of the file. Var-sign defaults to $ for this field.
    string name;

    /// The string that prefixes variable usage.
    string varSign;

    /// The text to put into the file. May use variables.
    string code;
}

/// Contains data about a library.
struct Library
{
    /// The name of the library.
    string      name;
    
    /// The path to code to put into the LibCode folder.
    string      codePath;
    
    /// The folder to the library files
    string      libraryFolder;
    
    /// The names of the libraries to include. These are absolute paths.
    string[]    libraries;

    /// The folder to the shared objects
    string      sharedFolder;

    /// The shared object to include. These are absolute paths
    string[]    sharedObjects;

    /// Fills out data about the library.
    /// 
    /// Parameters:
    ///     libraryFile = The name of the file, relative to "$exePath/Libraries/"
    this(string libraryFile)
    {
        auto exePath = thisExePath.dirName;
        auto root = parseFile(buildNormalizedPath(exePath, "Libraries", setExtension(libraryFile, "sdl")));

        // Read in the folder paths first, then we can bother with everything else
        this.getFolders(root, exePath);
        this.processInfo(root, exePath);
    }

    private
    {
        void processInfo(Tag root, string exePath)
        {
            foreach(tag; root.tags.filter!(tag => tag.name != "libraryFolder" && tag.name != "sharedObjectFolder"))
            {
                switch(tag.name)
                {
                    case "name":
                        this.name = tag.values[0].get!string;
                        break;

                    case "codePath":
                        this.codePath = buildNormalizedPath(exePath, "Libraries", tag.values[0].get!string);
                        break;

                    case "library":
                        this.libraries ~= buildNormalizedPath(exePath, "Libraries", 
                            this.libraryFolder, tag.values[0].get!string);
                        break;

                    case "sharedObject":
                        this.sharedObjects ~= buildNormalizedPath(exePath, "Libraries", 
                            this.sharedFolder, tag.values[0].get!string);
                        break;

                    default: 
                        throw new Exception("Unknown tag: " ~ tag.name);
                }
            }
        }

        void getFolders(Tag root, string exePath)
        {
            foreach(folder; root.tags.filter!(tag => tag.name == "libraryFolder" || tag.name == "sharedObjectFolder"))
            {
                if(folder.name == "libraryFolder")
                {
                    this.libraryFolder = buildNormalizedPath(
                        exePath,
                        "Libraries",
                        folder.values[0].get!string);
                }
                else
                {
                    this.sharedFolder = buildNormalizedPath(
                        exePath,
                        "Libraries",
                        folder.values[0].get!string);
                }
            }
        }
    }
}

/// Contains information about a generator.
struct Generator
{
    /// Contains variables the generator has set.
    /// Variables are used to inject text into certain things.
    string[string]  variables;

    /// The folders to create in the thing.
    Folder[]        folders;

    /// The files to create.
    File[]          files;

    /// Data for the generator.
    GeneratorData   data;

    /// Using the given data, read in a generator file and boobs.
    this(GeneratorData data)
    {
        // Parse the SDL
        auto root               = parseFile(data.getGenerator(data.input.generator).file);
        this.variables["name"]  = data.input.name;
        this.data               = data;

        // Then start to fill out everything
        this.generateVariables(root, data);
        this.processFolders(root, data);
        this.processFiles(root, data);
    }

    private
    {
        /// Goes over the root, and proccesses any tags that need to process GUID
        ///
        /// Parameters:
        ///     root = The root tag of the SDL code.
        void generateVariables(Tag root, GeneratorData data)
        {
            foreach(tag; root.tags.filter!(t => t.name == "guid" || t.name == "repeat"))
            {
                // Generate a guid
                if(tag.name == "guid")
                {
                    this.variables[tag.values[0].get!string] = randomUUID().toString();
                }

                // Repeat a pattern
                else
                {
                    this.repeatPattern(tag, data);
                }
            }
        }

        /// Repeats a pattern.
        void repeatPattern(Tag tag, GeneratorData data)
        {
            /*
             * Allowed values:
             * libraries = All the file names of the library files.
             * */
            
            // Read in all the things first before we even attempt to bother with it.
            auto pattern    = tag.attributes["pattern"][0].value.get!string;
            auto value      = tag.attributes["value"][0].value.get!string;
            auto name       = tag.attributes["name"][0].value.get!string;
            
            // Set values to what it should be
            string[] values;
            
            switch(value)
            {
                case "libraries":
                    foreach(lib; data.libraries)
                    {
                        values ~= lib.libraries.map!(str => str.baseName).array;
                    }
                    break;
                    
                default:
                    throw new Exception("Invalid repeat value: ", value);
            }
            
            // Then repeat the pattern
            auto finalPattern = "";
            foreach(value2; values)
            {
                this.variables["value"] = value2;
                finalPattern ~= this.replaceVariables("$", pattern);
            }
            
            this.variables[name] = finalPattern;
        }

        /// Process the full paths of folders.
        void processFolders(Tag root, GeneratorData data)
        {
            foreach(Tag folder; root.tags["folder"])
            {
                // To save headaches, paths are converted to their full path form as soon as we can.
                auto folderName = this.replaceVariables("$", folder.attributes["name"][0].value.get!string);
                auto folderType = "Normal";

                // If the folder specifies a type, then set it as so.
                foreach(type; folder.attributes.filter!(a => a.name == "type"))
                {
                    folderType = type.value.get!string;
                    break;
                }

                this.folders ~= Folder(buildNormalizedPath(data.projectDirectory, folderName), folderType);
            }
        }

        /// Processes the files.
        void processFiles(Tag root, GeneratorData data)
        {
            foreach(Tag file; root.tags["file"])
            {
                File toAdd;
                toAdd.name      = buildNormalizedPath(data.projectDirectory, 
                                        this.replaceVariables("$", file.attributes["name"][0].value.get!string));
                toAdd.varSign   = file.attributes["var-sign"][0].value.get!string;
                toAdd.code      = file.attributes["code"][0].value.get!string;

                // This is a slow, but easy method
                // Just go over every variable we have and try to replace it into the code.
                toAdd.code = this.replaceVariables(toAdd.varSign, toAdd.code);

                this.files ~= toAdd;
            }
        }

        /// Replaces all variables in the thing
        string replaceVariables(string varSign, string code)
        {
            foreach(key, value; this.variables)
            {
                code = code.replace((varSign ~ key), value);
            }

            return code;
        }
    }
}

/// Constructs a project from the data made from a generator.
struct Constructor
{
    private
    {
        Generator _generator;
    }

    /// Set up the constructor.
    this(Generator generator)
    {
        this._generator = generator;
    }

    /// Construct the project.
    void construct()
    {
        // If the folder already exists, just assume there's already something there.
        if(exists(this._generator.data.projectDirectory))
        {
            throw new Exception(format("Folder '%s' already exists. Delete it if you wish to create a project.",
                    this._generator.data.projectDirectory));
        }

        // First, setup the project's structure
        this.createStructure();

        // Then, write out the files
        this.writeFiles();

        // Finally, copy over every file.
        this.copyFiles();
    }

    private
    {
        void createStructure()
        {
            writefln("Creating project structure.");
            foreach(folder; this._generator.folders)
            {
                mkdirRecurse(folder.name);
            }
        }

        void writeFiles()
        {
            writefln("Creating project files.");
            foreach(file; this._generator.files)
            {
                write(file.name, file.code);
            }
        }

        void copyFiles()
        {
            writefln("Copying over library files.");
            // First, find all the special folders we need
            Folder   libs;
            Folder   libCode;
            Folder[] bins;
            foreach(folder; this._generator.folders)
            {
                if(folder.type == "Libs")
                {
                    libs = folder;
                }
                else if(folder.type == "LibCode")
                {
                    libCode = folder;
                }
                else if(folder.type == "Bin")
                {
                    bins ~= folder;
                }
            }
            enforce(libs.name != "", "Expected a folder of type: Libs");
            enforce(libCode.name != "", "Expected a folder of type: LibCode");

            // Then, start copying everything.
            foreach(library; this._generator.data.libraries)
            {
                copyFolder(library.codePath, libCode.name);

                foreach(lib; library.libraries)
                {
                    std.file.copy(lib, buildNormalizedPath(libs.name, lib.baseName),);
                }

                foreach(object; library.sharedObjects)
                {
                    foreach(bin; bins)
                    {
                        std.file.copy(object, buildNormalizedPath(bin.name, object.baseName));
                    }
                }
            }
        }

        /// Copies the entire directory at $(B folder) to $(B to).
        void copyFolder(string folder, string to)
        {       
            foreach(DirEntry entry; dirEntries(folder, SpanMode.shallow))
            {           
                // For other directories, just call this function again
                if(entry.isDir)
                {
                    auto New = buildNormalizedPath(to, entry.baseName);
                    
                    if(!exists(New))
                    {
                        mkdir(New);
                    }
                    
                    this.copyFolder(entry.name, New);
                }
                // For files, just copy them.
                else
                {
                    copy(entry.name, buildNormalizedPath(to, entry.baseName));
                }
            }
        }
    }
}

/// Reads in and returns the generators described in $(B file)
GeneratorFile[string] readGeneratorList(string file = "Generators/List.sdl")
{
    // Read in the root, then start to populate things.
    auto root = parseSource(std.file.readText(file), file);
    GeneratorFile[string] toReturn;

    // Because this is ran from he command line, these paths need to be relative to the exe's directory
    auto exePath = thisExePath().dirName();

    foreach(Tag tag; root.tags["generator"])
    {
        auto name       = tag.attributes["name"][0].value.get!string;
        auto fileName   = tag.attributes["file"][0];

        toReturn[name] = GeneratorFile(name, buildNormalizedPath(exePath, "Generators", fileName.value.get!(string)));
    }
    
    return toReturn;
}